package com.sunnyweather.android.logic.network

import com.sunnyweather.android.SunnyWeatherApplication
import com.sunnyweather.android.logic.model.DailyResponse
import com.sunnyweather.android.logic.model.RealtimeResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherService {

    //@GET("v2.5/${SunnyWeatherApplication.TOKEN}/{lng},{lat}/realtime.json")
    @GET("realtime.json")
    fun getRealtimeWeather(@Query("lng") lng: String, @Query("lat") lat: String): Call<RealtimeResponse>
//    fun getRealtimeWeather(@Path("lng") lng: String, @Path("lat") lat: String): Call<RealtimeResponse>

    //@GET("v2.5/${SunnyWeatherApplication.TOKEN}/{lng},{lat}/daily.json")
    @GET("daily.json")
    fun getDailyWeather(@Query("lng") lng: String, @Query("lat") lat: String): Call<DailyResponse>
//    fun getDailyWeather(@Path("lng") lng: String, @Path("lat") lat: String): Call<DailyResponse>

}